package microbenchmarks;

import org.RoaringTreeMap.Tree;
import org.hierarchicalroaringbitmap.LazyHierarRoaringBitmap;
import org.hierarchicalroaringbitmap.Roaring2levels64bits;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.*;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class Appends {
        @Param({ "50", "500", "5000", "50000", "500000", "5000000"})		
		static int N;
    	
        long[] set1, set2;
        ArrayList<Long> al;
        LinkedList<Long> ll;
        Roaring2levels64bits rb2lvl;
        Tree rbTreeMap;
        LazyHierarRoaringBitmap lazyRb;

        @Setup
        public void setup() {        
        	DataGenerator dg = new DataGenerator();        	
        	set1=dg.getUniform((double)N/(double)dg.getMax());
        }    

    @Benchmark
    public int AppendArrayList() {
    	al=new ArrayList<Long>();
    	for(int i=0; i< set1.length; i++)
    		al.add(set1[i]);
        int bogus = al.size();        
        return bogus;
    }

    @Benchmark
    public int AppendLinkedList() {
    	ll= new LinkedList<Long>();
    	for(int i=0; i< set1.length; i++)
    		ll.add(set1[i]);
        int bogus = ll.size();        
        return bogus;
    }
    
    @Benchmark
    public int AppendRoaring2lvl() {
    	rb2lvl = new Roaring2levels64bits();
    	for(int i=0; i< set1.length; i++)
    		rb2lvl.add(set1[i]);
        int bogus = rb2lvl.size();        
        return bogus;
    }
    
    @Benchmark
    public int AppendRoaringTreeMap() {
    	rbTreeMap = new Tree();
    	for(int i=0; i< set1.length; i++)
    		rbTreeMap.add(set1[i]);
        int bogus = rbTreeMap.size();        
        return bogus;
    }
    
    @Benchmark
    public int AppendLazyRoaring() {
    	lazyRb = new LazyHierarRoaringBitmap();
    	for(int i=0; i< set1.length; i++)
    		lazyRb.add(set1[i]);
        int bogus = lazyRb.size();        
        return bogus;
    }

    public static void main(final String[] args) throws RunnerException {
    	Options opt = new OptionsBuilder()
    	.include(".*" + Appends.class.getSimpleName() + ".*")
    	.warmupIterations(5).measurementIterations(5).forks(1)
    	.shouldDoGC(true)
    	.build();
    	new Runner(opt).run();
    }
}
