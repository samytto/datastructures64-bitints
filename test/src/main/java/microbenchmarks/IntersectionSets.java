package microbenchmarks;

import org.RoaringTreeMap.Tree;
import org.apache.lucene.util.OpenBitSet;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.LinkedList;
import org.hierarchicalroaringbitmap.LazyHierarRoaringBitmap;
import org.hierarchicalroaringbitmap.Roaring2levels64bits;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class IntersectionSets {
        @Param({ "50", "500", "5000", "50000", "500000", "5000000"})
		static int N;
    	
        long[] set1, set2;
        OpenBitSet obs1, obs2;
        HashSet<Long> hs1, hs2;
        TreeSet<Long> ts1, ts2;
        ArrayList<Long> al1, al2;
        LinkedList<Long> ll1, ll2;
        Roaring2levels64bits rb2lvl1, rb2lvl2;
        Tree rbTrMap1, rbTrMap2;
        LazyHierarRoaringBitmap lazyRb1, lazyRb2;

        @Setup
        public void setup() {        
        	DataGenerator dg = new DataGenerator();
        	obs1 = new OpenBitSet();
        	obs2= new OpenBitSet();
        	al1=new ArrayList<Long>(); 
        	al2= new ArrayList<Long>();
        	ll1= new LinkedList<Long>();
        	ll2=new LinkedList<Long>();
        	hs1 = new HashSet<Long>();
        	hs2 = new HashSet<Long>();
        	ts1 = new TreeSet<Long>(); 
        	ts2 = new TreeSet<Long>();
        	rb2lvl1 = new Roaring2levels64bits();
        	rb2lvl2 = new Roaring2levels64bits();
        	rbTrMap1 = new Tree();
        	rbTrMap2 = new Tree();
        	lazyRb1 = new LazyHierarRoaringBitmap();
        	lazyRb2 = new LazyHierarRoaringBitmap();
        	
        	set1=dg.getUniform((double)N/(double)dg.getMax());
        	set2=dg.getUniform((double)N/(double)dg.getMax());
        	
        	for(int i=0; i< set1.length; i++){
        		obs1.set(set1[i]);
        		al1.add(set1[i]);
        		ll1.add(set1[i]);
        		hs1.add(set1[i]);
        		ts1.add(set1[i]);
        		rb2lvl1.add(set1[i]);
        		rbTrMap1.add(set1[i]);
        		lazyRb1.add(set1[i]);
        	}
        	for(int i=0; i< set2.length; i++){
        		obs2.set(set2[i]);
        		al2.add(set2[i]);
        		ll2.add(set2[i]);
        		hs2.add(set2[i]);
        		ts2.add(set2[i]);
        		rb2lvl2.add(set2[i]);
        		rbTrMap2.add(set2[i]);
        		lazyRb2.add(set2[i]);
        	}        	
        }
    
    @Benchmark    
    public long intersectOpenBitSet() {
        OpenBitSet cpobs = (OpenBitSet) obs1.clone();
        cpobs.and(obs2);
        long bogus = cpobs.size();        
        return bogus;
    }    
        
    @Benchmark    
    public int intersectArrayList() {
    	ArrayList<Long> cpal = (ArrayList<Long>) al1.clone();
        cpal.retainAll(al2);
        int bogus = cpal.size();        
        return bogus;
    }

    @Benchmark    
    public int intersectLinkedList() {
    	LinkedList<Long> cpll = (LinkedList<Long>) ll1.clone();
    	cpll.retainAll(ll2);
        int bogus = cpll.size();        
        return bogus;
    }    

    @Benchmark    
    public int intersectHashSet() {
    	HashSet<Long> cphs = (HashSet<Long>) hs1.clone();
    	cphs.retainAll(hs2);
        int bogus = cphs.size();
        return bogus;
    }
    
    @Benchmark    
    public int intersectTreeSet() {
    	TreeSet<Long> cpts = (TreeSet<Long>) ts1.clone();
    	cpts.retainAll(ts2);
        int bogus = cpts.size();        
        return bogus;
    }
    
    @Benchmark    
    public int intersectRb2lvls() {
    	Roaring2levels64bits cprb2lvl = Roaring2levels64bits.and(rb2lvl1, rb2lvl2);
    	int bogus = cprb2lvl.getCardinality();        
        return bogus;
    }
    
    @Benchmark    
    public long intersectRbTreeMap() {
    	Tree cpRbTrMap = Tree.intersection(rbTrMap1, rbTrMap2);
        long bogus = cpRbTrMap.getCardinality();        
        return bogus;
    }
    
    @Benchmark    
    public int intersectLazyRB() {
    	LazyHierarRoaringBitmap lazyRb = LazyHierarRoaringBitmap.and(lazyRb1, lazyRb2);
    	int bogus = lazyRb.getCardinality();        
        return bogus;
    }
    
    public static void main(final String[] args) throws RunnerException {
    	Options opt = new OptionsBuilder()
    	.include(".*" + IntersectionSets.class.getSimpleName() + ".*")
    	.warmupIterations(5).measurementIterations(5).forks(1)
    	.shouldDoGC(true)
    	.build();
    	new Runner(opt).run();
    }
}
