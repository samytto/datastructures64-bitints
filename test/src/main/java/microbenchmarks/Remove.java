package microbenchmarks;

import org.RoaringTreeMap.Tree;
import org.apache.lucene.util.OpenBitSet;
import org.hierarchicalroaringbitmap.LazyHierarRoaringBitmap;
import org.hierarchicalroaringbitmap.Roaring2levels64bits;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.*;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class Remove {
	@Param({ "50", "500", "5000", "50000", "500000", "5000000"})
	static int N;
	
    long[] set;
    OpenBitSet obs;
    HashSet<Long> hs;
    TreeSet<Long> ts;
    ArrayList<Long> al;
    LinkedList<Long> ll;
    Roaring2levels64bits rb2lvl;
    Tree rbTrMap;
    LazyHierarRoaringBitmap lazyRb;

    @Setup
    public void setup() {        
    	DataGenerator dg = new DataGenerator();
    	obs = new OpenBitSet();
    	al=new ArrayList<Long>(); 
    	ll= new LinkedList<Long>();
    	hs = new HashSet<Long>();
    	ts = new TreeSet<Long>(); 
    	rb2lvl = new Roaring2levels64bits();
    	rbTrMap = new Tree();
    	lazyRb = new LazyHierarRoaringBitmap();
    	
    	set=dg.getUniform((double)N/(double)dg.getMax());
    	
    	for(int i=0; i< set.length; i++){
    		obs.set(set[i]);
    		al.add(set[i]);
    		ll.add(set[i]);
    		hs.add(set[i]);
    		ts.add(set[i]);
    		rb2lvl.add(set[i]);
    		rbTrMap.add(set[i]);
    		lazyRb.add(set[i]);
    	}        	
    }

@Benchmark    
public long removeOpenBitSet() {
    for(int i=set.length-1; i>=0; i++)
    	obs.clear(set[i]);
    long bogus = obs.size();        
    return bogus;
}    
    
@Benchmark    
public long removeArrayList() {
	for(int i=set.length-1; i>=0; i++)
    	al.remove(set[i]);
    long bogus = al.size();        
    return bogus;
}

@Benchmark    
public long removeLinkedList() {
	for(int i=set.length-1; i>=0; i++)
    	ll.remove(set[i]);
    long bogus = ll.size();        
    return bogus;
}    

@Benchmark    
public long removeHashSet() {
	for(int i=set.length-1; i>=0; i++)
    	hs.remove(set[i]);
    long bogus = hs.size();        
    return bogus;
}

@Benchmark    
public long removeTreeSet() {
	for(int i=set.length-1; i>=0; i++)
    	ts.remove(set[i]);
    long bogus = ts.size();        
    return bogus;
}

@Benchmark    
public long removeRb2lvls() {
	for(int i=set.length-1; i>=0; i++)
    	rb2lvl.remove(set[i]);
    long bogus = rb2lvl.size();        
    return bogus;
}

@Benchmark    
public long removeRbTreeMap() {
	for(int i=set.length-1; i>=0; i++)
    	rbTrMap.remove(set[i]);
    long bogus = rbTrMap.size();        
    return bogus;
}

@Benchmark    
public long removeLazyRB() {
	for(int i=set.length-1; i>=0; i++)
    	lazyRb.remove(set[i]);
    long bogus = lazyRb.size();        
    return bogus;
}

public static void main(final String[] args) throws RunnerException {
	Options opt = new OptionsBuilder()
	.include(".*" + IntersectionSets.class.getSimpleName() + ".*")
	.warmupIterations(5).measurementIterations(5).forks(1)
	.shouldDoGC(true)
	.build();
	new Runner(opt).run();
}
}
