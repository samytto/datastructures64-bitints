# Benchmarking data structures for storing 64-bit integers on synthetic data

License
------
This code is licensed under Apache License, Version 2.0 (ASL2.0).

Usage 
===================
* install java
* install maven 2
* execute : mvn package
* then : cd target
* then :     java -cp Bench64-bitDataStructures-0.0.1-SNAPSHOT.jar:lib/* -javaagent:../lib/SizeOf.jar Main
* Also, please think to set an appropriate maximum Java heap size and a JVM's stack size using the following JVM parameters : -Xmx and -Xss respectively.
