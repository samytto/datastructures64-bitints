import java.util.ArrayList;
import java.util.LinkedList;


public class Util {
	
	protected static int toIntUnsigned(short x) {
        return x & 0xFFFF;
    }
	
	/**
     * Unite two sorted array lists and write the result to the provided
     * output array
     *
     * @param set1    first array
     * @param length1 length of first array
     * @param set2    second array
     * @param length2 length of second array
     * @param buffer  output array
     * @return cardinality of the union
     */
    public static ArrayList<Long> unsignedUnion2by2(final ArrayList<Long> set1, 
    		final ArrayList<Long> set2) {        
        int length1= set1.size(), length2= set2.size();
        if (0 == length2) {
            return (ArrayList<Long>) set1.clone();
        }
        if (0 == length1) {
            return (ArrayList<Long>) set2.clone();
        }
        int k1 = 0, k2 = 0;
        ArrayList<Long> res = new ArrayList<Long>();
        while (true) {
        	long s1= set1.get(k1), s2= set2.get(k2);  
            if (s1 < s2) {
                res.add(s1);
                ++k1;
                if (k1 >= length1) {
                    for (; k2 < length2; ++k2)
                        res.add(set2.get(k2));
                    break;
                }
            } else if (s1 == s2) {
                res.add(s1);
                ++k1;
                ++k2;
                if (k1 >= length1) {
                    for (; k2 < length2; ++k2)
                        res.add(set2.get(k2));
                    break;
                }
                if (k2 >= length2) {
                    for (; k1 < length1; ++k1)
                        res.add(set1.get(k1));
                    break;
                }
            } else {// if (set1[k1]>set2[k2])
                res.add(s2);
                ++k2;
                if (k2 >= length2) {
                    for (; k1 < length1; ++k1)
                        res.add(set1.get(k1));
                    break;
                }
            }
        }
        return res;
    }
    
    /**
     * Unite two sorted linked lists and write the result to the provided
     * output array
     *
     * @param set1    first array
     * @param length1 length of first array
     * @param set2    second array
     * @param length2 length of second array
     * @param buffer  output array
     * @return cardinality of the union
     */
    public static LinkedList<Long> unsignedUnion2by2(final LinkedList<Long> set1, 
    		final LinkedList<Long> set2) {        
        int length1= set1.size(), length2= set2.size();
        if (0 == length2) {
            return (LinkedList<Long>) set1.clone();
        }
        if (0 == length1) {
            return (LinkedList<Long>) set2.clone();
        }
        int pos = 0;
        int k1 = 0, k2 = 0;
        LinkedList<Long> res = new LinkedList<Long>();
        while (true) {
        	long s1= set1.get(k1), s2= set2.get(k2);  
            if (s1 < s2) {
                res.add(s1);
                ++k1;
                if (k1 >= length1) {
                    for (; k2 < length2; ++k2)
                        res.add(set2.get(k2));
                    break;
                }
            } else if (s1 == s2) {
                res.add(s1);
                ++k1;
                ++k2;
                if (k1 >= length1) {
                    for (; k2 < length2; ++k2)
                        res.add(set2.get(k2));
                    break;
                }
                if (k2 >= length2) {
                    for (; k1 < length1; ++k1)
                        res.add(set1.get(k1));
                    break;
                }
            } else {// if (set1[k1]>set2[k2])
                res.add(s2);
                ++k2;
                if (k2 >= length2) {
                    for (; k1 < length1; ++k1)
                        res.add(set1.get(k1));
                    break;
                }
            }
        }
        return res;
    }

}
