/*
 * (c) Samy Chambi and Daniel Lemire
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.TreeSet;

import org.RoaringTreeMap.Tree;
import org.hierarchicalroaringbitmap.LazyHierarRoaringBitmap;
import org.hierarchicalroaringbitmap.Roaring2levels64bits;

import OpenBiSet.OpenBitSet;
import net.sourceforge.sizeof.SizeOf;

/**
 * 
 * This a reproduction of the benchmark used by Colantonio and Di Pietro,
 * Concise: Compressed 'n' Composable Integer Set.
 * 
 * While they report "Compression" as the ratio between the number of 32-bit
 * words required to represent the compressed bitmap and the cardinality of the
 * integer set, we report the number of bits per integer.
 * 
 * Like them, we use "Density" mean the ratio between the cardinality of the set
 * and the number range.
 * 
 * Like them, we "Max/Cardinality" to mean the ratio between the maximal value
 * (i.e., the number range) and the cardinality of the set that is, the inverse
 * of the density.
 * 
 * 
 * Time measurement are expressed in nanoseconds. Each experiment is performed
 * 100 times, and the average reported.
 * 
 * @author Daniel Lemire
 * 
 */
public class Main {

	 	private static int bogus = 0;
	
        /**
         * @param a
         *                an array of integers
         * @return a bitset representing the provided integers
         */
        public static OpenBitSet toOpenBitSet(final long[] v) {
                OpenBitSet obs = new OpenBitSet();         
                for(long x : v)
                	obs.set(x);
                return obs;
        }                   
        
        /**
         * @param a
         *                an array of integers
         * @return a an ArrayList representing the provided long integers
         */
        public static ArrayList<Long> toArrayList(final long[] a) {
                ArrayList<Long> al = new ArrayList<Long>();
                for (long x : a)
                        al.add(x);
                return al;
        }

        /**
         * @param a
         *                an array of integers
         * @return a a LinkedList representing the provided long integers
         */
        public static LinkedList<Long> toLinkedList(final long[] a) {
                LinkedList<Long> ll = new LinkedList<Long>();
                for (long x : a)
                        ll.add(x);
                return ll;
        }
        
        /**
         * @param a
         *                an array of integers
         * @return a HashSet representing the provided long integers
         */
        public static HashSet<Long> toHashSet(final long[] a) {
                HashSet<Long> hs = new HashSet<Long>();
                for (long x : a)
                        hs.add(x);
                return hs;
        }
        
        /**
         * @param a
         *                an array of integers
         * @return a HashSet representing the provided long integers
         */
        public static TreeSet<Long> toTreeSet(final long[] a) {
                TreeSet<Long> ts = new TreeSet<Long>();
                for (long x : a)
                        ts.add(x);
                return ts;
        }
        
        /*public static Object[] toPerfectArray(Object[] a){
        	HashSet hs = new HashSet();
        	//Omitting recurrences
        	for(int i=0; i<a.length; i++)
        		hs.add(a[i]);
        	Object[] r = hs.toArray();
        	Arrays.sort(r);
        	return r;
        }*/
        
    private static long[] readFile(String fileName){
        BufferedReader br = null;
        long[] res=null;
		try {
			br = new BufferedReader(new FileReader(fileName));		
            String line = br.readLine();
            String[] longs= line.split(",");
            res= new long[longs.length];
            for(int i=0; i<longs.length; i++)
            	res[i]= Long.parseLong(longs[i], 10);            
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
            try {
				br.close();
			} catch (IOException e) {e.printStackTrace();}
        }
		return res;
	}
        /**
         * @param args
         *                command line arguments
         */
        public static void main(final String[] args) {
                Locale.setDefault(Locale.US);
                System.out
                        .println("# This benchmark emulates what Colantonio and Di Pietro,");
                System.out
                        .println("#  did in Concise: Compressed 'n' Composable Integer Set");
                System.out.println("########");
                System.out.println("# " + System.getProperty("java.vendor")
                        + " " + System.getProperty("java.version") + " "
                        + System.getProperty("java.vm.name"));
                System.out.println("# " + System.getProperty("os.name") + " "
                        + System.getProperty("os.arch") + " "
                        + System.getProperty("os.version"));
                System.out.println("# processors: "
                        + Runtime.getRuntime().availableProcessors());
                System.out.println("# max mem.: "
                        + Runtime.getRuntime().maxMemory()/1024);
                System.out.println("########");
                int N = 100000;
                boolean sizeof = true;
                try {
                        SizeOf.setMinSizeToLog(0);
                        SizeOf.skipStaticField(true);
                        SizeOf.deepSizeOf(args);
                }catch (IllegalStateException e) {
                        sizeof = false;
                        System.out.println("# disabling sizeOf, run  -javaagent:lib/SizeOf.jar or equiv. to enable");
                }
                DataGenerator gen = new DataGenerator(N);
                int TIMES = 100;
                gen.setUniform();
                System.out.println("# uniform: dry run");
                test(gen, false, TIMES, sizeof); //dry run
                System.out.println("# uniform: start counting performances");
                test(gen, true, TIMES, sizeof);
                System.out.println();
                gen.setZipfian();
                System.out.println("# Zipf: dry run");
                test(gen, false, TIMES, sizeof); //dry run
                System.out.println("# Zipf: start counting performances");
                test(gen, true, TIMES, sizeof);
                //gen.setSeededUniform();
                //test(gen, false, TIMES, sizeof); //dry run
                //System.out.println("# chunked: start counting benchmark performances");
                //test(gen, true, TIMES, sizeof);
                //System.out.println();
        }

        /**
         * @param gen
         *                data generator
         * @param verbose
         *                whether to print out the result
         * @param TIMES
         *                how many times should we run each test
         * @param sizeof
         *                whether to use the sizeOf library.
         */
        public static void test(final DataGenerator gen, final boolean verbose, final int TIMES, boolean sizeof) {
                if (!verbose)
                        System.out
                                .println("# running a dry run (can take a long time)");                                
                launchBench(TIMES, gen, sizeof, verbose);
                System.out.println("#ignore = " + bogus);
        }
        
        public static void launchBench(int TIMES, final DataGenerator gen, boolean sizeof, boolean verbose) {
         long bef, aft;
         DecimalFormat df = new DecimalFormat("0.000E0");
         DecimalFormat dfb = new DecimalFormat("000.0");
         String distribution = gen.is_zipfian()?"Zipfian":"uniform";
         PrintWriter pw1 = null, pw2 = null;
         //String oBitSetName="OpBitS", rrName = "Rr64bit", AlistName="ArrayL", lListName="LinkedL", hSetName="HashS", tSetName="TreeS";
         String[] schemesNames = {"OpBitS", "ArrayL", "LinkedL", "HashS", "TreeS", "RrTreeMap", "rb2lvls", "lazyRb"};
         //try {
				//pw1 = new PrintWriter(distribution+"_array1.txt","UTF-8");
				//pw2 = new PrintWriter(distribution+"_array2.txt","UTF-8");
			//} catch (FileNotFoundException | UnsupportedEncodingException e) {
				//e.printStackTrace();
			//}                 	
        	if(verbose) 
        		System.out.println("\nRoaring bitmap/array conversion threshold =\n");        	
        	if (verbose)
                if (gen.is_zipfian())
                        System.out.println("### zipfian test");
                //else if(gen.is_seededUniform())
                  //  System.out.println("### chunked uniform test");
                else
                	System.out.println("### uniform test");
        	if (verbose)
                System.out
                        .println("# first columns are timings [intersection times in ns], then append times in ns, "
                                + "then remove times in ns, then bits/int, then union times");
        	if (verbose && sizeof)
                System.out
                        .println("# For size (last columns), first column is estimated, second is sizeof");
        	/*if (verbose)
                System.out
                        .print("# density\t"+oBitSetName+"\t\t"+AlistName+"\t\t"+lListName+"\t\t"+hSetName+"\t\t"+tSetName+"\t\t"+rrName
                                +   "\t\t\t"+oBitSetName+"\t\t"+AlistName+"\t\t"+lListName+"\t\t"+hSetName+"\t\t"+tSetName+"\t\t"+rrName
                                +   "\t\t\t"+oBitSetName+"\t\t"+AlistName+"\t\t"+lListName+"\t\t"+hSetName+"\t\t"+tSetName+"\t\t"+rrName);
        	if (verbose)
                if (sizeof)
                        System.out
                                .println("\t\t"+oBitSetName+"\t"+oBitSetName+"\t"+AlistName+"\t"+AlistName+"\t"
                                		+lListName+"\t"+lListName+"\t"+hSetName+"\t"+hSetName
                                		+"\t"+tSetName+"\t"+tSetName+"\t"+rrName+"\t"+rrName);
                else
                        System.out
                                .println("\t\t"+oBitSetName+"\t\t"+AlistName+"\t\t"+lListName+"\t\t"+hSetName+"\t\t"+tSetName+"\t\t"+rrName);*/
        	
        	for (double d=0.000000001; d<=0.0001; d*=10){
        	int nbSchemes = schemesNames.length;
                double[] timings = new double[nbSchemes];
                double[] unions = new double[nbSchemes];
                double[] storageinbits = new double[nbSchemes];
                double[] truestorageinbits = new double[nbSchemes];
                double[] appendTimes = new double[nbSchemes];
                double[] removeTimes = new double[nbSchemes];   
                
                for(int times = 0; times < TIMES; ++times) {
                
                	long[] v1 = gen.getRandomArray(d);
                    long[] v2 = gen.getRandomArray(d);
                
                //if(verbose) {
                  //System.out.println("Data were gnerated. Distribution= "+distribution+". Density = "+d+". v1.length= "+v1.length+". v2.length= "+v2.length);
                  //pw1.println("\nAnalyzing array : density = "+d+", distribution : "+distribution);
                  //pw2.println("\nAnalyzing array : density = "+d+", distribution : "+distribution);
                  //analyzeSortedIntsArray(v1, "v1", pw1);
                  //analyzeSortedIntsArray(v2, "v2", pw2);
                //}                
                        
//######################// OpenBitSet
                        int schemNum = 0;
                        //System.out.println("OpenBitSet. Density = "+d);
                		//System.out.println("Append times");
                        // Append times
                        bef = System.nanoTime();
                        OpenBitSet borig1 = toOpenBitSet(v1);// we will clone it
                        aft = System.nanoTime();
                        bogus += borig1.size();
                        appendTimes[schemNum] += aft - bef;                        
                        // And times.
                        //System.out.println("And times.");
                        bef = System.nanoTime();
                        OpenBitSet b1and = (OpenBitSet) borig1.clone(); // for fair
                                                             // comparison
                                                             // (not
                                                             // inplace)                        
                        aft = System.nanoTime();
                        long timeToClone = aft-bef;
                        borig1=null;
                        OpenBitSet b2 = toOpenBitSet(v2);
                        bogus += b2.size();
                        //Storage
                        //System.out.println("Calculating the storage.");
                        storageinbits[schemNum] += b1and.size() + b2.size();
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(b1and)
                                		//.deepSizeOf(borig1)
                                        + SizeOf.deepSizeOf(b2)) * 8;
                        //And suite...
                        bef = System.nanoTime();
                        b1and.and(b2);
                        aft = System.nanoTime();
                        timings[schemNum] += (aft - bef)+timeToClone;
                        bogus += b1and.size();
                        b2=null;                       						
                        // OR times.
                        //System.out.println("OR times.");
                        borig1=toOpenBitSet(v1);                        
                        bef = System.nanoTime();
                        OpenBitSet b1u = (OpenBitSet) borig1.clone(); // for
                                                              // fair
                                                              // comparison
                                                              // (not
                                                              // inplace)
                        aft = System.nanoTime();
                        timeToClone = aft-bef;
                        borig1=null;
                        v1=null;
                        b2 = toOpenBitSet(v2);
                        bef = System.nanoTime();
                        b1u.or(b2);
                        aft = System.nanoTime();
                        unions[schemNum] += (aft - bef)+timeToClone;
                        bogus += b1u.size();
                        b1u=null;
                        v2=null;   
                        //Object[] trueunion = verbose? null : toArray(b1u);                        
                        // Remove times.
                        //System.out.println("Remove times.");
                        //long toRemove=0;
                        //toRemove = v2[gen.getRand().nextInt(gen.getN())];
                        //bef = System.nanoTime();
                        //b2.clear(toRemove);
                        //aft = System.nanoTime();
                        //removeTimes[schemNum] += aft - bef;
                        //bogus += borig1.size();
                        //System.out.println("Ending.");
                        //Object[] b2withremoval = verbose? null : toArray(b2);
                        b2 = null;
//######################// ArrayList
                        schemNum++;                        
                        // Append times
                        //System.out.println("Array List. Append.");
                        bef = System.nanoTime();
                        ArrayList<Long> al1 = toArrayList(v1);
                        aft = System.nanoTime();
                        bogus += al1.size();
                        appendTimes[schemNum] += aft - bef;
                        ArrayList<Long> al2 = toArrayList(v2);
                        bogus += al2.size();
                        storageinbits[schemNum] += al1.size() * 64;
                        storageinbits[schemNum] += al2.size()*64;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(al1)
                                        + SizeOf.deepSizeOf(al2)) * 8;
                        //Intersection
                        //System.out.println("Intersection.");                        
                        bef = System.nanoTime();
                        ArrayList<Long> al1i = (ArrayList<Long>) al1.clone();
                        al1i.retainAll(al2);//O(n^2) time
                        aft = System.nanoTime();                        
                        bogus += al1i.size();
                        timings[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose){
                      	  // HashSet hs = new HashSet(al1i);
                       	  // Object[] a = hs.toArray();                      	   
                     	   //Arrays.sort(a);
                      	   //if(!Arrays.equals(trueintersection, a)) 
                          	//	throw new RuntimeException("bug");
                        //}
                        al1i=null;
                        //Union
                        //System.out.println("Union.");
                        bef = System.nanoTime();
                        ArrayList<Long> al1u = (ArrayList<Long>)al1.clone();
                        al1u.addAll(al2);
                        //ArrayList<Long> al1u = Util.unsignedUnion2by2(al1, al2);
                        aft = System.nanoTime();                                
                        bogus += al1u.size();
                        unions[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose) {
                      	  // HashSet hs = new HashSet(al1u);
                       	   //Object[] a = hs.toArray();                      	   
                     	   //Arrays.sort(a);
                      	   //if(!Arrays.equals(trueunion, a)) 
                          	//	throw new RuntimeException("bug");
                        //}
                        // Removal times
                        //System.out.println("Remove.");
                        //bef = System.nanoTime();
                        //al2.remove(toRemove);
                        //aft = System.nanoTime();
                        //removeTimes[schemNum] += aft - bef;
                        //if(!verbose) 
                        	//if (!Arrays.equals(b2withremoval, al2.toArray()))
                              //  throw new RuntimeException("bug");
                        //bogus += al2.size();                        
                        //Object[] trueintersection = verbose? null : toPerfectArray(al1i.toArray());
                        //Object[] trueunion = verbose? null : toPerfectArray(al1u.toArray());*/
                        al1 = null;
                        al2 = null;
                        //al1u = null;
//######################// LinkedList
                        schemNum++;
                        //System.out.println("LinkedList. Density = "+d);
                        // Append times
                        //System.out.println("Append.");
                        bef = System.nanoTime();
                        LinkedList<Long> ll1 = toLinkedList(v1);
                        aft = System.nanoTime();
                        bogus += ll1.size();
                        appendTimes[schemNum] += aft - bef;
                        LinkedList<Long> ll2 = toLinkedList(v2);
                        bogus += ll2.size();
                        // Storage
                        storageinbits[schemNum] += ll1.size() * 64;
                        storageinbits[schemNum] += ll2.size() * 64;
                        if (sizeof) 
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(ll1)
                                        + SizeOf.deepSizeOf(ll2)) * 8;
                        // Intersect times
                        bef = System.nanoTime();
                        LinkedList<Long> ll1i = (LinkedList<Long>) ll1.clone();
                        ll1i.retainAll(ll2);
                        aft = System.nanoTime();
                        bogus += ll1i.size();
                        timings[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose) {
                     	//    HashSet hs = new HashSet(ll1i);
                        //    Object[] a = hs.toArray();
                        //    Arrays.sort(a);
                        //    if(!Arrays.equals(trueintersection, a))
                         //                throw new RuntimeException("bug");
                        //}
                        ll1i=null;
                        // Union times
                        bef = System.nanoTime();
                        LinkedList<Long> ll1u = (LinkedList<Long>) ll1.clone();
                        ll1u.addAll(ll2);
                        //LinkedList<Long> ll1u = Util.unsignedUnion2by2(ll1, ll2);
                        aft = System.nanoTime();
                        bogus += ll1u.size();
                        unions[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose) {
                      	//    HashSet hs = new HashSet(ll1u);
                        //    Object[] a = hs.toArray();
                        //    Arrays.sort(a);
                         //   if(!Arrays.equals(trueunion, a))
                        //                 throw new RuntimeException("bug");
                        //}
                        // Removing times
                        //bef = System.nanoTime();
                        //ll2.remove(toRemove);
                        //aft = System.nanoTime();
                        //removeTimes[schemNum] += aft - bef;
                        //bogus += ll2.size();
                        //if(!verbose) 
                        //	if (!Arrays.equals(b2withremoval, ll2.toArray()))
                        //       throw new RuntimeException("bug");*/
                        ll1 = null;
                        ll2 = null;
                        //ll1u = null;
//######################// HashSet
                        schemNum++;
                        //System.out.println("HashSet. Density = "+d);
                        // Append times
                        //System.out.println("Append.");
                        bef = System.nanoTime();
                        HashSet<Long> hs1 = toHashSet(v1);
                        aft = System.nanoTime();
                        bogus += hs1.size();
                        appendTimes[schemNum] += aft - bef;
                        HashSet<Long> hs2 = toHashSet(v2);
                        bogus += hs2.size();
                        // Storage
                        storageinbits[schemNum] += hs1.size() * 64;
                        storageinbits[schemNum] += hs2.size() * 64;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(hs1)
                                        + SizeOf.deepSizeOf(hs2)) * 8;
                        // Intersect times
                        //System.out.println("Intersect.");
                        bef = System.nanoTime();
                        HashSet<Long> hs1i = (HashSet<Long>) hs1.clone();
                        hs1i.retainAll(hs2);
                        aft = System.nanoTime();
                        bogus += hs1i.size();
                        timings[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose) {
                     	//   Object[] a = hs1i.toArray();
                     	//   Arrays.sort(a);
                        //	if(!Arrays.equals(trueintersection, a)) 
                        // 		throw new RuntimeException("bug");
                        //}
                        hs1i=null;
                        // Union times
                        //System.out.println("Union.");
                        bef = System.nanoTime();
                        HashSet<Long> hs1u = (HashSet<Long>) hs1.clone();
                        hs1u.addAll(hs2);
                        aft = System.nanoTime();
                        bogus += hs1u.size();
                        unions[schemNum] += aft - bef;
                        // we verify the answer
                        //if(!verbose) {
                        //	Object[] obj= hs1u.toArray();
                        //	Arrays.sort(obj);
                     	//    if(!Arrays.equals(trueunion, obj)) 
                        // 		throw new RuntimeException("bug");
                        //}
                        // Removing times
                        //System.out.println("Remove.");
                        //bef = System.nanoTime();
                        //hs2.remove(toRemove);
                        //aft = System.nanoTime();
                        //removeTimes[schemNum] += aft - bef;
                        //bogus += hs2.size();
                     // we verify the answer
                        //if(!verbose){
                        	//Object[] obj= hs2.toArray();
                        	//Arrays.sort(obj);
                     	   	//if(!Arrays.equals(b2withremoval, obj))                          		
                     		  // throw new RuntimeException("bug");
                        //}*/
                        hs1 = null;
                        hs2 = null;                      
                        //hs1u = null;
//######################//TreeSet
                        schemNum++;
                        //System.out.println("TreeSet. Density = "+d);
                        //Append times
                        //System.out.println("Append.");
                        bef = System.nanoTime();
                        TreeSet<Long> ts1 = toTreeSet(v1);
                        aft = System.nanoTime();
                        bogus += ts1.size();
                        appendTimes[schemNum] += aft - bef;
                        TreeSet<Long> ts2 = toTreeSet(v2);
                        bogus += ts2.size();
                        // Storage
                        storageinbits[schemNum] += ts1.size() * 64;
                        storageinbits[schemNum] += ts2.size() * 64;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(ts1)
                                        + SizeOf.deepSizeOf(ts2)) * 8;
                        // Intersect times
                        //System.out.println("Intersect.");
                        bef = System.nanoTime();
                        TreeSet<Long> ts1i = (TreeSet<Long>)ts1.clone();
                        ts1i.retainAll(ts2);
                        aft = System.nanoTime();
                        bogus += ts1i.size();
                        timings[schemNum] += aft - bef;
                        //Verifying the answer
                        //if(!verbose) 
                      	//   if(!Arrays.equals(trueintersection, ts1i.toArray())) 
                         // 		throw new RuntimeException("bug");
                        ts1i=null;
                        // Union times
                        //System.out.println("Union.");
                        bef = System.nanoTime();
                        TreeSet<Long> ts1u = (TreeSet<Long>) ts1.clone();
                        ts1u.addAll(ts2);
                        aft = System.nanoTime();
                        bogus += ts1u.size();
                        unions[schemNum] += aft - bef;
                        //Verifying the answer
                        //if(!verbose) 
                       	//   if(!Arrays.equals(trueunion, ts1u.toArray())) 
                        //   		throw new RuntimeException("bug");
                        // Removing times
                        //System.out.println("Remove.");
                        //bef = System.nanoTime();
                        //ts2.remove(toRemove);
                        //aft = System.nanoTime();
                        //removeTimes[schemNum] += aft - bef;
                      //Verifying the answer
                        //if(!verbose) 
                       	  // if(!Arrays.equals(b2withremoval, ts2.toArray())) 
                           	//	throw new RuntimeException("bug");
                        //bogus += ts2.size();*/
                        ts1 = null;
                        ts2 = null;                 
                        //ts1u = null;
//######################//RoaringTreeMap64bits
                        schemNum++;
                        //System.out.println("RoaringTreeMap. Density = "+d);
                        //System.out.println("Append");
                        Tree rtm1 = null;
                        Tree rtm2 = null;
                        //Add times
                        bef = System.nanoTime();
                        rtm1 = Tree.bitmapOf(v1);
                        aft = System.nanoTime();
                        bogus += rtm1.size();
                        appendTimes[schemNum] += aft - bef;
                        rtm2 = Tree.bitmapOf(v2);
                        bogus += rtm2.size();
                        // Storage
                        //System.out.println("Storage");
                        storageinbits[schemNum] += 0;
                        storageinbits[schemNum] += 0;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(rtm1)
                                        + SizeOf.deepSizeOf(rtm2)) * 8;
                        // Intersect times
                        //System.out.println("Intersect");
                        Tree rtm1and=null;
                        bef = System.nanoTime();
                        rtm1and = Tree.intersection(rtm1, rtm2);
                        aft = System.nanoTime();
                        // we verify the answer
                       //if(!verbose) 
                    	 //  if(!Arrays.equals(trueintersection, rtm1and.toArray())) 
                        //		throw new RuntimeException("bug");
                        bogus += rtm1and.size();
                        timings[schemNum] += aft - bef;
                        rtm1and=null;
                        // Union times
                        //System.out.println("Union");
                        Tree rtm1or = null;
                        bef = System.nanoTime();
                        rtm1or = Tree.union(rtm1, rtm2);
                        aft = System.nanoTime();
                        bogus += rtm1or.size();
                        // we verify the answer
                        //if(!verbose)                 
                        //	if(!Arrays.equals(trueunion, rtm1or.toArray()))
                        //		throw new RuntimeException("bug");
                        unions[schemNum] += aft - bef;
                        rtm1or=null;
                        // Remove times
                        //System.out.println("Removes");
                        //bef = System.nanoTime();
                        //rtm2.remove(toRemove);
                        //aft = System.nanoTime();
                        //if(!verbose) 
                        	//if (!Arrays.equals(b2withremoval, rtm2.toArray()))
                              //  throw new RuntimeException("bug");
                        //removeTimes[schemNum] += aft - bef;
                        //bogus += rtm2.size();*/
                        rtm1=null;
						rtm2=null;
						//rtm1or=null;
//######################//Roaring64bits2levels
                        schemNum++;
                        //System.out.println("Roaring64bits2levels. Density = "+d);
                        Roaring2levels64bits r2l1 = null;
                        Roaring2levels64bits r2l2 = null;
                        // Add times
                        //System.out.println("Append.");
                        bef = System.nanoTime();
                        r2l1 = Roaring2levels64bits.bitmapOf(v1);
                        aft = System.nanoTime();
                        v1=null;
                        bogus += r2l1.size();
                        appendTimes[schemNum] += aft - bef;
                        r2l2 = Roaring2levels64bits.bitmapOf(v2);
                        bogus += r2l2.size();
                        v2=null;
                        // Storage
                        //System.out.println("Storage");
                        storageinbits[schemNum] += 0;
                        storageinbits[schemNum] += 0;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(r2l1)
                                        + SizeOf.deepSizeOf(r2l2)) * 8;
                        // Intersect times
                        //System.out.println("Intersect");
                        Roaring2levels64bits r2l1and=null;
                        bef = System.nanoTime();
                        r2l1and = Roaring2levels64bits.and(r2l1, r2l2);
                        aft = System.nanoTime();
                        // we verify the answer
                       //if(!verbose) 
                    	//   if (!Arrays.equals(trueintersection, r2l1and.toArray())) 
                        //		throw new RuntimeException("bug");
                        bogus += r2l1and.size();
                        timings[schemNum] += aft - bef;
                        r2l1and=null;
                        // Union times
                        //System.out.println("Union.");
                        Roaring2levels64bits r2l1or = null;
                        bef = System.nanoTime();
                        r2l1or = Roaring2levels64bits.or(r2l1, r2l2);
                        aft = System.nanoTime();
                        bogus += r2l1or.size();
                        // we verify the answer
                        //if(!verbose)
                        //	if (!Arrays.equals(trueunion, r2l1or.toArray()))
                        //        	throw new RuntimeException("bug");
                        unions[schemNum] += aft - bef;
                        r2l1or=null;
                        r2l1=null;
                        r2l2=null;
                        // Remove times
                        //bef = System.nanoTime();
                        //rtm2.remove(toRemove);
                        //aft = System.nanoTime();
                        //if(!verbose) 
                        	//if (!Arrays.equals(r2l2.toArray(), b2withremoval))
                              //  throw new RuntimeException("bug");
                        //removeTimes[6] += aft - bef;
                        //bogus += rtm2.size();                  
//######################//LazyHierarchicalRoaringBitmap
                        schemNum++;
                        //System.out.println("LazyHierarch64-bitRoaring. Density = "+d);
                        LazyHierarRoaringBitmap lzhrb1 = null;
                        LazyHierarRoaringBitmap lzhrb2 = null;
                        //Add times
                        //System.out.println("Append.");
                        bef = System.nanoTime();
                        lzhrb1 = LazyHierarRoaringBitmap.bitmapOf(v1);
                        aft = System.nanoTime();
                        bogus += lzhrb1.getCardinality();
                        appendTimes[schemNum] += aft - bef;
                        lzhrb2 = LazyHierarRoaringBitmap.bitmapOf(v2);
                        bogus += lzhrb2.getCardinality();
                        // Storage
                        //System.out.println("Storage.");
                        storageinbits[schemNum] += 0;
                        storageinbits[schemNum] += 0;
                        if (sizeof)
                                truestorageinbits[schemNum] += (SizeOf
                                        .deepSizeOf(lzhrb1)
                                        + SizeOf.deepSizeOf(lzhrb2)) * 8;
                        // Intersect times
                        //System.out.println("Intersect.");
                        LazyHierarRoaringBitmap lzhrb1and=null;
                        bef = System.nanoTime();
                        lzhrb1and = LazyHierarRoaringBitmap.and(lzhrb1, lzhrb2);
                        aft = System.nanoTime();
                        // we verify the answer
                        //if(!verbose) 
                        //	if (!Arrays.equals(trueintersection, lzhrb1and.toArray())) 
                        //		throw new RuntimeException("bug");
                        bogus += lzhrb1and.getCardinality();
                        timings[schemNum] += aft - bef;
                        lzhrb1and=null;
                        // Union times
                        //System.out.println("Union.");
                        LazyHierarRoaringBitmap lzhrb1or = null;
                        bef = System.nanoTime();
                        lzhrb1or = LazyHierarRoaringBitmap.or(lzhrb1, lzhrb2);
                        aft = System.nanoTime();
                        bogus += lzhrb1or.getCardinality();
                        // we verify the answer
                       // if(!verbose)
                        //	if (!Arrays.equals(trueunion, lzhrb1or.toArray()))
                        //        	throw new RuntimeException("bug");
                        unions[schemNum++] += aft - bef;
                        lzhrb1or=null;
                        // Remove times
                        //bef = System.nanoTime();
                        //lzhrb2.remove(toRemove);
                        //aft = System.nanoTime();
                        //if(!verbose) 
                        //if (!Arrays.equals(rb2.toArray(), b2withremoval))
                          //      throw new RuntimeException("bug");
                        //removeTimes[6] += aft - bef;
                        //bogus += hb1.getCardinality();*/
                        lzhrb1=null;
                        lzhrb2=null;
                }
                if (verbose) {
                	//intersection times
                	System.out.print(df.format(d) +"\t"+schemesNames[0]+"\t"+df.format(timings[0] / TIMES));
                	for(int i=1; i<nbSchemes; i++)
                		System.out.print("\t\t"+schemesNames[i]+"\t"+df.format(timings[i] / TIMES));
                    //append times
                	System.out.print("\t\t\t\t"+schemesNames[0]+"\t"+df.format(appendTimes[0]
                                / (TIMES * gen.getN())));
                    for(int i=1; i<nbSchemes; i++)
                        System.out.print("\t\t"+schemesNames[i]+"\t"+df.format(appendTimes[i]
                                / (TIMES * gen.getN())));
                    //remove times
                    System.out.print("\t\t\t\t"+schemesNames[0]+"\t"+df.format(removeTimes[0] / TIMES));
                    for(int i=1; i<nbSchemes; i++)
                    	System.out.print("\t\t"+schemesNames[i]+"\t"+df.format(removeTimes[i] / TIMES));
                }
                if (verbose)
                        if (sizeof) {//size
                                System.out
                                        .print("\t\t\t\t"
                                                +schemesNames[0]+"\t"+dfb.format(truestorageinbits[0]
                                                        / (2 * TIMES * gen.getN())));
                                for(int i=1; i<nbSchemes; i++)
                                	System.out.print("\t"
                                                +schemesNames[i]+"\t"+ dfb.format(truestorageinbits[i]
                                                        / (2 * TIMES * gen.getN())));
                        }
                        else {
                                System.out.print("\t\t\t\t"
                                        +schemesNames[0]+"\t"+dfb.format(storageinbits[0]
                                                / (2 * TIMES * gen.getN())));
                				for(int i=1; i<nbSchemes; i++)
                                        System.out.print("\t\t"
                                        +schemesNames[i]+"\t"+dfb.format(storageinbits[i]
                                                / (2 * TIMES * gen.getN())));
                        }
                if (verbose) {//union times
                        System.out.print("\t\t\t\t"+schemesNames[0]+"\t"+df.format(unions[0] / TIMES));
                        for(int i=1; i<nbSchemes; i++)
                        	System.out.print("\t\t"+schemesNames[i]+"\t"+df.format(unions[i] / TIMES));
                }
                if(verbose) System.out.println();
        }     
       // pw1.close();
       // pw2.close();
  }
        
        private static void analyzeSortedIntsArray(long[] A, String ArrayName, PrintWriter pw) {        	
        	//HashSet<Long> hsfstl = new HashSet<Long>();
        	//HashSet<Long> hsSndl = new HashSet<Long>();
        	//pw.println("Printing the "+ArrayName+" array ::");
        	for(long x : A)
        		pw.print(x+",");
        	pw.println();
        	/*for(int i=0; i<A.length; i++) {
        		hsfstl.add((byte)(A[i]>>24));
        		hsSndl.add((byte)(A[i]>>16));
        	}
        	//Printing first level chunks
        	pw.println("Printing the first level chunks");
        	for(byte b : hsfstl)
        		pw.print(b+", ");
        	//Printing second level chunks
        	pw.println("\n\nPrinting the second level chunks");
        	for(byte b : hsSndl)
        		pw.print(b+", ");
        	pw.println();*/
        	pw.close();
        }
        
        public static boolean equals(Object[] a, Object[] a2) {
        if (a==a2)
            return true;
        if (a==null || a2==null)
            return false;

        int length = a.length;
        if (a2.length != length){
            System.out.println("arrays length is different. a.length= "+a.length+", a2.lentgh= "+a2.length);
        	return false;
        }
        for(int i=0; i<length; i++) {
            Object o1 = a[i];
            Object o2 = a2[i];
            if (!(o1==null ? o2==null : o1.equals(o2))) {
            	System.out.println("Values are different :: o= "+o1+", o2= "+o2);
            	return false;
            }
        }
        return true;
   }

        private static Object[] toArray(final OpenBitSet bs) {
                ArrayList<Long> a = new ArrayList<Long>();
                for (long x = bs.nextSetBit(0L); x >= 0; x = bs.nextSetBit(x + 1L))
                    a.add(x);
                Object[] res = a.toArray();
                Arrays.sort(res);
                return res;
        }		

}
