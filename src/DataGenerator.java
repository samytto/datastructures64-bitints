/*
 * (c) Samy Chambi and daniel Lemire.
 */

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

/**
 * 
 * This is a data generator object reproducing the models used
 * by Colantonio and Di Pietro, Concise: Compressed n Composable Integer Set
 * 
 * @author Daniel Lemire
 *
 */
public class DataGenerator {
        private Random rand = new Random();
        private int N;
        private long max = 50000000000L;
        //private long max = 500000000L;
        boolean zipfian = false;

        /**
         * Will generate arrays with default size (100000)
         */
        public DataGenerator() {
                N = 100000;
        }
        
        /**
         * @param n size of the arrays
         */
        public DataGenerator(final int n) {
                if(n<1) throw new IllegalArgumentException("number of ints should be positive");
                N = n;
        }
        
        /**
         * @return whether the data generator is in zipfian mode
         */
        public final boolean is_zipfian() {
                return zipfian;
        }
        
        /**
         * set the data generator in uniform  mode
         */
        public final void setUniform() {
                zipfian = false;
        }
        
        /**
         * set the data generator in Zipfian mode
         */
        public final void setZipfian() {
                zipfian = true;
        }
        
        public long getMax() {
        	return max;
        }
        
        public int getN() {
        	return N;
        }
        
        public Random getRand() {
        	return rand;
        }
        
        /**
         * Generate a random array (sorted integer set)
         * 
         * @param d should vary from 0 to 0.999
         * @return an array with a uniform or zipfian distribution
         */
        public final long[] getRandomArray(double d) {
                if(zipfian)
                        return getZipfian(d);
                return getUniform(d);
        }
        
        public final long[] getCommonInts(int n, long[] v) {
        	HashSet<Long> hs = new HashSet<Long>();
        	Random rand = new Random();
        	while(hs.size()<n)
        		hs.add(v[rand.nextInt(v.length)]);
        	long[] l = new long[n];
        	int c=0;
        	for(long x : hs)
        		l[c++]=x;
        	Arrays.sort(l);
        	return l;
        }
        
        /**
         * Generate a random array (sorted integer set)
         * 
         * @param d should vary from 0 to 1.000
         * @return an array with a uniform distribution
         */
        public final long[] getUniform(double d) {
                ////////////////// (from arxiv version)
                //at each generation of a pseudo-random number a in [0, 1), 
                //in uniform sets an integer corresponding to floor(a * max) was added,
                //where max = 
                //105/d by varying d (the density) from 0.001 to 0.999.
                //////////////////        		
                if((d<0) || (d>1.000)) 
                	throw new IllegalArgumentException("parameter should be in [0.005,0.999]"); 
                
                N  = (int) (d*max);
                if(d>=0.99) {
                    long[] answer = new long[N];
                    for(int k = 0; k<N;++k)                      
                    	answer[k] = k;
                    return answer;
                }
                //System.out.println("Uniform :: N= "+N);
                final HashSet<Long> hash = new HashSet<Long>();
                while(hash.size()<N) {
                    double a= rand.nextDouble();
                	long e = (long) (a * max);
                	hash.add(e);
                }               
                int c = 0;
                long[] answer = new long[N];
                for(long x : hash)
                   answer[c++] = x;
                Arrays.sort(answer);                
                return answer;
        }
        
        /**
         * Generate a random array (sorted integer set). 
         * 
         * This is what Colantonio and Di Pietro called Zipfian.
         * 
         * @param d should vary from 0 to 1.000
         * @return an array with a "Zipfian" distribution
         */
        public final long[] getZipfian(double d) {
                ////////////////// (from arXiv version)
                //Similarly, in Zipfian sets, at each number generation, an 
                //integer corresponding to floor(max * a^4) was added, where
                //max in [1.2 * 10^ 5, 6 * 10^9]. In this way, we generated skewed
                //data such that most of the integers were concentrated to lower 
                //values, while numbers with high values were very sparse.                 
                //////////////////
                ////////////////
                // in  Colantonio and Di Pietro's arXiv version of their  paper at the 
                // end of page 3, they clearly state that it is a^4
                // However, the version that they published (IPL) states a^2.
                ////////////////    
        	if((d<0) || (d>1.000)) 
            		throw new IllegalArgumentException("parameter should be in [0.005,0.999]"); 
            	N = (int)(max*d);
            	if(d>=0.99) {
                	long[] answer = new long[N];
                	for(int k = 0; k<N;++k)                      
                		answer[k] = k;
                	return answer;
            	}                
                //System.out.println("Zipfian :: N= "+N);
                HashSet<Long> hash = new HashSet<Long>();
                int loopcount = 0;
                while(hash.size()<N) {
                	double a= rand.nextDouble();
                	long e = (long) (a*a * max);
                        hash.add(e);
                        if(loopcount++ > 10*N) 
                        	throw new RuntimeException("zipfian generation is too expensive");
                }
                int c = 0;
                long[] answer = new long[N];
                for(long x : hash)
                   answer[c++] = x;
                Arrays.sort(answer);
                return answer;
        }
        
        public static void main(final String[] args) {
        	DataGenerator dg = new DataGenerator();
        	double d=0.001;
        	long vZipf[] = dg.getZipfian(d);
        	long vUniform[] = dg.getUniform(d);//1<<24
        	
        	//long v3[] = DataGenerator.getChunkedUniform(0.0001, 1<<24);
        	PrintWriter pw1=null, pw2=null;
        	System.out.println("Making files");
        	try {
				pw1 = new PrintWriter("UniformData","UTF-8");
				pw2 = new PrintWriter("ZipfianData","UTF-8");
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
        	pw1.println("Printing distinct high 32 bits v1 uniform :");
        	for(int i=0; i<vUniform.length; i++)
        		pw1.print((vUniform[i]>>32)+", ");
        	pw1.println("\nPrinting distinct high 48 bits v1 uniform :");
        	for(int i=0; i<vUniform.length; i++)
        		pw1.print((vUniform[i]>>16)+", ");
        	//pw1.println("\nPrinting long integers v1 :");
        	//for(int i=0; i<v1.length; i++)
        		//pw1.print(v1[i]+", ");
        	pw1.close();        	
        	pw2.println("Printing distinct high 32 bits v2 zipf :");
        	for(int i=0; i<vZipf.length; i++)
        		pw2.print((vZipf[i]>>32)+", ");        	
        	pw2.println("\nPrinting distinct high 48 bits v2 zipf :");
        	for(int i=0; i<vZipf.length; i++)
        		pw2.print((vZipf[i]>>16)+", "); 
        	//pw2.println("Printing long integers v2 :");
        	//for(int i=0; i<v2.length; i++)
        		//pw2.print(v2[i]+", ");
        	pw2.close();        	
        	/*pw3.println("Printing first level v3 zipf :");
        	for(int i=0; i<v3.length; i++)
        		pw3.print(((v3[i]>>32)&0xFFFFFFFF)+", ");        	
        	pw3.println("Printing integers v3 :");
        	for(int i=0; i<v3.length; i++)
        		pw3.print(v3[i]+", ");
        	pw3.close();*/
        }
}
